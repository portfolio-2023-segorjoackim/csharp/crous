﻿using Dao;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace Views
{
    public partial class FAddEditD : Form
    {
        private DaoDepense daoDepense = new DaoDepense();
        private DaoColoc daoColoc = new DaoColoc();
        private Controlleur control = new Controlleur();
        private string fonction;
        private int index;
        private string nameFile;
        public FAddEditD(string fonction = "ajouter",int index = 0)
        {
            InitializeComponent();
            btn_parcourir.Click += Btn_parcourir_Click;
            btn_envoyer.Click += Btn_envoyer_Click;
            this.index = index;
            this.fonction = fonction;
            foreach(Colocataire coloc in daoColoc.getAllColoc())
            {
                string text = string.Format("Id : {2} Nom : {0} Prénom : {1}",coloc.Nom,coloc.Prenom,coloc.Id);
                cb_coloc.Items.Add(text);
            }
            switch(this.fonction)
            {
                case "ajouter":
                    lbl_title.Text = "Ajouter une dépense à : ";
                    btn_envoyer.Text = "Ajouter";
                    break;
                case "modifier":
                    lbl_title.Text = "Modifier la dépense de : ";
                    btn_envoyer.Text = "Modifer";
                    Depense laDepense = daoDepense.getAllDepense()[this.index];
                    Colocataire sonColoc = daoColoc.getColocataire(laDepense.Coloc.Id);
                    cb_coloc.Text = string.Format("Id : {2} Nom : {0} Prénom : {1}", sonColoc.Nom, sonColoc.Prenom, sonColoc.Id);
                    DateTime date = (DateTime)dt_date.Value;
                    tb_text.Text = laDepense.Text;
                    tb_montant.Text = laDepense.Montant.ToString();
                    this.nameFile = laDepense.Justificatif;
                    pb_image.ImageLocation = this.nameFile;
                    break;

            }
        }

        private void Btn_envoyer_Click(object sender, EventArgs e)
        {
            bool  valide = false;
            switch (this.fonction)
            {
                case "ajouter":

                    if (control.validationDepense(tb_text.Text,tb_montant.Text))
                    {
                        DateTime date =(DateTime)dt_date.Value;
                        decimal montant = Convert.ToDecimal(tb_montant.Text);
                        Colocataire Coloc = daoColoc.getAllColoc()[cb_coloc.SelectedIndex];
                        Depense depense = new Depense(date, (string)tb_text.Text, string.Format("{0}", this.nameFile), montant,Coloc);
                        daoDepense.addDepense(depense);
                        valide= true;
                        break;

                    }
                    else
                    {
                        MessageBox.Show("Il y a une erreur dans l'un des champs du fomulaire!");
                        valide = false;
                        break;
                    }
                    
                case "modifier":
                    if (control.validationDepense(tb_text.Text, tb_montant.Text))
                    {
                        Depense laDepense = daoDepense.getAllDepense()[this.index];
                        laDepense.Date = (DateTime)dt_date.Value;
                        laDepense.Montant = Convert.ToDecimal(tb_montant.Text);
                        laDepense.Justificatif = this.nameFile;
                        laDepense.Coloc = daoColoc.getAllColoc()[cb_coloc.SelectedIndex];
                        daoDepense.editDepense(laDepense);
                        valide= true; 
                        break;

                    }
                    else
                    {
                        MessageBox.Show("Il y a une erreur dans l'un des champs du fomulaire!");
                        valide = false;
                        break;
                    }
                        
                    

            }
            if (valide)
            {
                FEnregistrerD fEnregistrerD = new FEnregistrerD();
                this.Hide();
                fEnregistrerD.Show();
            }
        }

        private void Btn_parcourir_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "JPEG|*.jpg|JFIF|*.jfif|PNG|*.png";
            openFileDialog1.ShowDialog();
            if (openFileDialog1.FileName != null)
            {
                pb_image.ImageLocation = openFileDialog1.FileName;
                this.nameFile = openFileDialog1.FileName.Replace("\\", "/");
                
            }
        }

    }
}
