﻿using Dao;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Views
{
    public partial class FEnregistrerD : Form
    {
        private DaoDepense daoDepense = new DaoDepense();
        private DaoColoc daoColoc  = new DaoColoc();
        public FEnregistrerD()
        {
            InitializeComponent();
            btn_AddDepenses.Click += Btn_AddDepenses_Click;
            btn_EditDepenses.Click += Btn_EditDepenses_Click;
            btn_DeleteDepense.Click += Btn_DeleteDepense_Click;
            if (daoDepense.getAllDepense().Count != 0)
            {
                foreach (Depense depense in daoDepense.getAllDepense())
                {
                    this.lb_Depenses.Items.Add(depense);
                }
            }
        }

        private void Btn_DeleteDepense_Click(object sender, EventArgs e)
        {
            if(lb_Depenses.SelectedIndex > -1)
            {
                Depense laDepense = daoDepense.getAllDepense()[lb_Depenses.SelectedIndex];
                daoDepense.getAllDepense().Remove(laDepense);
                daoDepense.deleteDepense(laDepense.Id);
                this.Hide();
                FEnregistrerD fEnregistrerD = new FEnregistrerD();
                fEnregistrerD.Show();
            }
        }

        private void Btn_EditDepenses_Click(object sender, EventArgs e)
        {
            if(lb_Depenses.SelectedIndex > -1)
            {
                FAddEditD fAddEditD = new FAddEditD("modifier",lb_Depenses.SelectedIndex);
                this.Hide();
                fAddEditD.Show();
            }
            
        }

        private void Btn_AddDepenses_Click(object sender, EventArgs e)
        {
            FAddEditD fAddEditD = new FAddEditD();
            this.Hide();
            fAddEditD.Show();
        }

    }
}
