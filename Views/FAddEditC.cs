﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Schema;
using Dao;
using Model;

namespace Views
{
    public partial class FAddEditC : Form
    {
        private DaoColoc daoColoc = new DaoColoc();
        private Controlleur control = new Controlleur();
        private string fonction;
        private int index;
        public FAddEditC(string fonction = "ajouter",int index = 0)
        {
            this.index = index;
            this.fonction = fonction;
            InitializeComponent();
            switch (this.fonction)
            {
                case "ajouter":
                    this.Text = "Ajout d'un colocataire";
                    label1.Text = "Ajouter un colocataire : ";
                    this.btn_Valider.Text = "Ajouter";
                    break;
                case "modifier":
                    this.Text = "Modifer le colocataire";
                    label1.Text = "Modifier votre colocataire : ";
                    this.btn_Valider.Text = "Modifier";
                    Colocataire leColoc = daoColoc.getAllColoc()[this.index];
                    tb_nom.Text = leColoc.Nom;
                    tb_prenom.Text = leColoc.Prenom;
                    tb_mail.Text = leColoc.Mail;
                    tb_telephone.Text = leColoc.Telephone;
                    break;
            }
            this.FormClosed += FAddEditC_FormClosed; ;
            
            this.btn_Valider.Click += Btn_Valider_Click;
        }

        private void FAddEditC_FormClosed(object sender, FormClosedEventArgs e)
        {
            FGerer gerer = new FGerer();
            gerer.Show();
        }

        private void Btn_Valider_Click(object sender, EventArgs e)
        {
            bool valide = false;
            switch (this.fonction)
            {
                case "ajouter":
                    if (control.validationColoc(tb_nom.Text, tb_prenom.Text, tb_mail.Text, tb_telephone.Text))
                    {
                        Colocataire coloc = new Colocataire(tb_nom.Text, tb_prenom.Text, tb_mail.Text, tb_telephone.Text);
                        daoColoc.addColoc(coloc);
                        valide = true;
                        string name = Dns.GetHostName();
                        string address = Dns.GetHostByName(name).AddressList[0].ToString();
                        Log log = new Log("ajouter un nouveau colocataire", address, DateTime.Now);
                        new DaoLog().addLog(log);
                        break;
                    }
                    else
                    {
                        MessageBox.Show("Il y a une erreur dans l'un des champs du formulaire!");
                        valide = false;
                        break;
                    }
                    
                    
                case "modifier":
                    if (control.validationColoc(tb_nom.Text, tb_prenom.Text, tb_mail.Text, tb_telephone.Text))
                    {
                        Colocataire leColoc = daoColoc.getAllColoc()[this.index];
                        leColoc.Nom = tb_nom.Text;
                        leColoc.Prenom = tb_prenom.Text;
                        leColoc.Mail = tb_mail.Text;
                        leColoc.Telephone = tb_telephone.Text;
                        daoColoc.editColoc(leColoc);
                        valide = true;
                        break;
                    }
                    else
                    {
                        MessageBox.Show("Il y a une erreur dans l'un des champs du formulaire!");
                        valide = false;
                        break;
                    }
                    
            }
            if (valide)
            { 
                this.Hide();
                FGerer gerer = new FGerer();
                gerer.Show();
            }
        }
    }
}
