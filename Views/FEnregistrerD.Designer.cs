﻿namespace Views
{
    partial class FEnregistrerD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lb_Depenses = new System.Windows.Forms.ListBox();
            this.btn_DeleteDepense = new System.Windows.Forms.Button();
            this.btn_EditDepenses = new System.Windows.Forms.Button();
            this.btn_AddDepenses = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Consultation des dépenses : ";
            // 
            // lb_Depenses
            // 
            this.lb_Depenses.FormattingEnabled = true;
            this.lb_Depenses.Location = new System.Drawing.Point(12, 65);
            this.lb_Depenses.Name = "lb_Depenses";
            this.lb_Depenses.Size = new System.Drawing.Size(424, 277);
            this.lb_Depenses.TabIndex = 1;
            // 
            // btn_DeleteDepense
            // 
            this.btn_DeleteDepense.Location = new System.Drawing.Point(316, 366);
            this.btn_DeleteDepense.Name = "btn_DeleteDepense";
            this.btn_DeleteDepense.Size = new System.Drawing.Size(120, 32);
            this.btn_DeleteDepense.TabIndex = 2;
            this.btn_DeleteDepense.Text = "Supprimer";
            this.btn_DeleteDepense.UseVisualStyleBackColor = true;
            // 
            // btn_EditDepenses
            // 
            this.btn_EditDepenses.Location = new System.Drawing.Point(163, 366);
            this.btn_EditDepenses.Name = "btn_EditDepenses";
            this.btn_EditDepenses.Size = new System.Drawing.Size(120, 32);
            this.btn_EditDepenses.TabIndex = 2;
            this.btn_EditDepenses.Text = "Modifier";
            this.btn_EditDepenses.UseVisualStyleBackColor = true;
            // 
            // btn_AddDepenses
            // 
            this.btn_AddDepenses.Location = new System.Drawing.Point(12, 366);
            this.btn_AddDepenses.Name = "btn_AddDepenses";
            this.btn_AddDepenses.Size = new System.Drawing.Size(120, 32);
            this.btn_AddDepenses.TabIndex = 2;
            this.btn_AddDepenses.Text = "Ajouter";
            this.btn_AddDepenses.UseVisualStyleBackColor = true;
            // 
            // FEnregistrerD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(448, 426);
            this.Controls.Add(this.btn_AddDepenses);
            this.Controls.Add(this.btn_EditDepenses);
            this.Controls.Add(this.btn_DeleteDepense);
            this.Controls.Add(this.lb_Depenses);
            this.Controls.Add(this.label1);
            this.Name = "FEnregistrerD";
            this.Text = "FEnregistrerD";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lb_Depenses;
        private System.Windows.Forms.Button btn_DeleteDepense;
        private System.Windows.Forms.Button btn_EditDepenses;
        private System.Windows.Forms.Button btn_AddDepenses;
    }
}