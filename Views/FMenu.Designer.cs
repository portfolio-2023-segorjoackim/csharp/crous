﻿namespace Views
{
    partial class FMenu
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMenu));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_Gerer = new System.Windows.Forms.Button();
            this.btn_Repartir = new System.Windows.Forms.Button();
            this.btn_Register = new System.Windows.Forms.Button();
            this.btn_Solde = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 73);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(132, 134);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btn_Gerer
            // 
            this.btn_Gerer.BackColor = System.Drawing.Color.OrangeRed;
            this.btn_Gerer.Location = new System.Drawing.Point(190, 73);
            this.btn_Gerer.Name = "btn_Gerer";
            this.btn_Gerer.Size = new System.Drawing.Size(133, 29);
            this.btn_Gerer.TabIndex = 1;
            this.btn_Gerer.Text = "Gérer les coloctaires";
            this.btn_Gerer.UseVisualStyleBackColor = false;
            // 
            // btn_Repartir
            // 
            this.btn_Repartir.BackColor = System.Drawing.Color.OrangeRed;
            this.btn_Repartir.Location = new System.Drawing.Point(190, 108);
            this.btn_Repartir.Name = "btn_Repartir";
            this.btn_Repartir.Size = new System.Drawing.Size(133, 29);
            this.btn_Repartir.TabIndex = 1;
            this.btn_Repartir.Text = "Répartir les dépenses";
            this.btn_Repartir.UseVisualStyleBackColor = false;
            // 
            // btn_Register
            // 
            this.btn_Register.BackColor = System.Drawing.Color.OrangeRed;
            this.btn_Register.Location = new System.Drawing.Point(190, 143);
            this.btn_Register.Name = "btn_Register";
            this.btn_Register.Size = new System.Drawing.Size(133, 29);
            this.btn_Register.TabIndex = 1;
            this.btn_Register.Text = "Enregistrer les dépenses";
            this.btn_Register.UseVisualStyleBackColor = false;
            // 
            // btn_Solde
            // 
            this.btn_Solde.BackColor = System.Drawing.Color.OrangeRed;
            this.btn_Solde.Location = new System.Drawing.Point(190, 178);
            this.btn_Solde.Name = "btn_Solde";
            this.btn_Solde.Size = new System.Drawing.Size(133, 29);
            this.btn_Solde.TabIndex = 1;
            this.btn_Solde.Text = "Solde période";
            this.btn_Solde.UseVisualStyleBackColor = false;
            // 
            // FMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(335, 279);
            this.Controls.Add(this.btn_Solde);
            this.Controls.Add(this.btn_Register);
            this.Controls.Add(this.btn_Repartir);
            this.Controls.Add(this.btn_Gerer);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FMenu";
            this.Text = "Menu";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_Gerer;
        private System.Windows.Forms.Button btn_Repartir;
        private System.Windows.Forms.Button btn_Register;
        private System.Windows.Forms.Button btn_Solde;
    }
}

