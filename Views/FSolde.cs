﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
using Dao;

namespace Views
{
    public partial class FSolde :FRepartition
    {
        private DaoDepense daoDepense = new DaoDepense();
        public FSolde()
        {
            InitializeComponent();
            this.btn_Repartir.Click += Btn_Repartir_Click;
        }

        private void Btn_Repartir_Click(object sender, EventArgs e)
        {
            Remplir();
            foreach (Depense d in daoDepense.getAllDepense())
            {
                if(d.Reparti == false)
                {
                    d.Reparti = true;
                    daoDepense.editDepense(d);
                }
            }
        }
    }
}
