﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Views
{
    public partial class FMenu : Form
    {
        public FMenu()
        {
            InitializeComponent();
            this.btn_Gerer.Click += Btn_Gerer_Click;
            this.btn_Register.Click += Btn_Register_Click;
            this.btn_Repartir.Click += Btn_Repartir_Click;
            this.btn_Solde.Click += Btn_Solde_Click;
        }

        private void Btn_Solde_Click(object sender, EventArgs e)
        {
            FSolde fSolde = new FSolde();
            fSolde.Show();
        }

        private void Btn_Repartir_Click(object sender, EventArgs e)
        {
            FRepartition repartition = new FRepartition();
            repartition.Show();
        }

        private void Btn_Register_Click(object sender, EventArgs e)
        {
            FEnregistrerD enregistrerD = new FEnregistrerD();
            enregistrerD.Show();

        }

        private void Btn_Gerer_Click(object sender, EventArgs e)
        {
            FGerer gerer = new FGerer();
            gerer.Show();
        }

    }
}
