﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao;
using Model;

namespace Views
{
    public partial class FRepartition : Form
    {
        private DaoColoc daoColoc = new DaoColoc();
        private DaoDepense daoDepense = new DaoDepense();
        public FRepartition()
        {
            InitializeComponent();
            btn_Calculer.Click += Btn_Calculer_Click;
            foreach (Colocataire c in daoColoc.getAllColoc())
            {
                decimal price = daoDepense.PriceTotalColoc(c);
                string text = string.Format("Le colocataire : {0} {1} a dépensé au total : {2}€", c.Nom, c.Prenom, price);
                lb_depensesC.Items.Add(text);
            }
        }


        private void Btn_Calculer_Click(object sender, EventArgs e)
        {
            lb_repartition.Items.Clear();
            label3.Text = daoDepense.Repartition().ToString("0.00");
            Remplir();
            
        }
        protected void Remplir()
        {
            lb_repartition.Items.Clear();
            foreach(Colocataire c in daoColoc.getAllColoc())
            {
                decimal douer = daoDepense.Repartition() - daoDepense.PriceTotalColoc(c);
                if(douer <= 0)
                {
                    string text = string.Format("Le colocataire : {0} {1} aurait du payer : {2}€ et doit : 0€", c.Nom, c.Prenom,label3.Text );
                    lb_repartition.Items.Add(text);
                }
                else
                {
                    string text = string.Format("Le colocataire : {0} {1} aurait du payer : {2}€ et doit : {3}€", c.Nom, c.Prenom, label3.Text,douer.ToString("0.00"));
                    lb_repartition.Items.Add(text);
                }
                
            }
        }

    }
}
