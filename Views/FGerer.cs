﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
using Dao;

namespace Views
{
    
    public partial class FGerer : Form
    {
        private DaoColoc daoColoc = new DaoColoc();
        public FGerer()
        {

            InitializeComponent();
            this.btn_Add.Click += Btn_Add_Click;
            this.btn_Edit.Click += Btn_Edit_Click;
            this.btn_Delete.Click += Btn_Delete_Click;
            
            if (daoColoc.getAllColoc().Count != 0)
            {
                foreach (Colocataire coloc in daoColoc.getAllColoc())
                {
                    //this.lb_Coloc.Items.Add(String.Format("Nom : {0} / Prénom : {1} / Mail : {2} / Téléphone : {3}", coloc.Nom, coloc.Prenom, coloc.Mail, coloc.Telephone));
                    this.lb_Coloc.Items.Add(coloc);
                }
            }
            
            
        }

        private void Btn_Delete_Click(object sender, EventArgs e)
        {
            if(lb_Coloc.SelectedIndex > -1)
            {
                Colocataire leColoc = daoColoc.getAllColoc()[lb_Coloc.SelectedIndex];
                daoColoc.getAllColoc().Remove(leColoc);
                daoColoc.deleteColoc(leColoc.Id);
                this.Hide();
                FGerer fGerer = new FGerer();
                fGerer.Show();
            }
        }

        private void Btn_Edit_Click(object sender, EventArgs e)
        {
            if(lb_Coloc.SelectedIndex > -1)
            {
                FAddEditC editC = new FAddEditC("modifier",lb_Coloc.SelectedIndex);
                this.Hide();
                editC.Show();
            }
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            FAddEditC ajoutC = new FAddEditC();
            this.Hide();
            ajoutC.Show();
        }
    }
}
