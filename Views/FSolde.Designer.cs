﻿namespace Views
{
    partial class FSolde
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Repartir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Repartir
            // 
            this.btn_Repartir.Location = new System.Drawing.Point(306, 396);
            this.btn_Repartir.Name = "btn_Repartir";
            this.btn_Repartir.Size = new System.Drawing.Size(92, 28);
            this.btn_Repartir.TabIndex = 7;
            this.btn_Repartir.Text = "Répartir";
            this.btn_Repartir.UseVisualStyleBackColor = true;
            // 
            // FSolde
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 450);
            this.Controls.Add(this.btn_Repartir);
            this.Name = "FSolde";
            this.Text = "FSolde";
            this.Controls.SetChildIndex(this.btn_Repartir, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Repartir;
    }
}