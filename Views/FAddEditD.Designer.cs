﻿namespace Views
{
    partial class FAddEditD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_title = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_text = new System.Windows.Forms.TextBox();
            this.tb_montant = new System.Windows.Forms.TextBox();
            this.pb_image = new System.Windows.Forms.PictureBox();
            this.btn_parcourir = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btn_envoyer = new System.Windows.Forms.Button();
            this.cb_coloc = new System.Windows.Forms.ComboBox();
            this.dt_date = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.pb_image)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Date de la dépense :";
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.Location = new System.Drawing.Point(12, 24);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(244, 25);
            this.lbl_title.TabIndex = 1;
            this.lbl_title.Text = "Ajouter une dépense :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 18);
            this.label5.TabIndex = 1;
            this.label5.Text = "Intitulé : ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 240);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 18);
            this.label6.TabIndex = 1;
            this.label6.Text = "Justificatif :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 172);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 18);
            this.label7.TabIndex = 1;
            this.label7.Text = "Montant :";
            // 
            // tb_text
            // 
            this.tb_text.Location = new System.Drawing.Point(89, 120);
            this.tb_text.Name = "tb_text";
            this.tb_text.Size = new System.Drawing.Size(100, 20);
            this.tb_text.TabIndex = 2;
            // 
            // tb_montant
            // 
            this.tb_montant.Location = new System.Drawing.Point(89, 172);
            this.tb_montant.Name = "tb_montant";
            this.tb_montant.Size = new System.Drawing.Size(100, 20);
            this.tb_montant.TabIndex = 2;
            // 
            // pb_image
            // 
            this.pb_image.BackColor = System.Drawing.SystemColors.Control;
            this.pb_image.Location = new System.Drawing.Point(227, 120);
            this.pb_image.Name = "pb_image";
            this.pb_image.Size = new System.Drawing.Size(294, 305);
            this.pb_image.TabIndex = 3;
            this.pb_image.TabStop = false;
            // 
            // btn_parcourir
            // 
            this.btn_parcourir.Location = new System.Drawing.Point(107, 239);
            this.btn_parcourir.Name = "btn_parcourir";
            this.btn_parcourir.Size = new System.Drawing.Size(75, 23);
            this.btn_parcourir.TabIndex = 4;
            this.btn_parcourir.Text = "Parcourir";
            this.btn_parcourir.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btn_envoyer
            // 
            this.btn_envoyer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_envoyer.Location = new System.Drawing.Point(212, 461);
            this.btn_envoyer.Name = "btn_envoyer";
            this.btn_envoyer.Size = new System.Drawing.Size(75, 23);
            this.btn_envoyer.TabIndex = 5;
            this.btn_envoyer.Text = "Envoyer";
            this.btn_envoyer.UseVisualStyleBackColor = true;
            // 
            // cb_coloc
            // 
            this.cb_coloc.FormattingEnabled = true;
            this.cb_coloc.Location = new System.Drawing.Point(262, 28);
            this.cb_coloc.Name = "cb_coloc";
            this.cb_coloc.Size = new System.Drawing.Size(243, 21);
            this.cb_coloc.TabIndex = 6;
            // 
            // dt_date
            // 
            this.dt_date.Location = new System.Drawing.Point(227, 78);
            this.dt_date.Name = "dt_date";
            this.dt_date.Size = new System.Drawing.Size(182, 20);
            this.dt_date.TabIndex = 7;
            // 
            // FAddEditD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 496);
            this.Controls.Add(this.dt_date);
            this.Controls.Add(this.cb_coloc);
            this.Controls.Add(this.btn_envoyer);
            this.Controls.Add(this.btn_parcourir);
            this.Controls.Add(this.pb_image);
            this.Controls.Add(this.tb_montant);
            this.Controls.Add(this.tb_text);
            this.Controls.Add(this.lbl_title);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Name = "FAddEditD";
            this.Text = "FGererD";
            ((System.ComponentModel.ISupportInitialize)(this.pb_image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_text;
        private System.Windows.Forms.TextBox tb_montant;
        private System.Windows.Forms.PictureBox pb_image;
        private System.Windows.Forms.Button btn_parcourir;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btn_envoyer;
        private System.Windows.Forms.ComboBox cb_coloc;
        private System.Windows.Forms.DateTimePicker dt_date;
    }
}