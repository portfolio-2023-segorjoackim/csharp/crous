﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Tests
{
    [TestClass()]
    public class ControlleurTests
    {
        [TestMethod()]
        public void prixTest()
        {
            bool actual = new Controlleur().prix("a,95");
            bool expected = false;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void mailTest()
        {
            bool actuel = new Controlleur().mail("jojo.segor@gmail.com");
            bool expected = true;
            Assert.AreEqual(expected, actuel);
        }

        [TestMethod()]
        public void telephoneTest() 
        {
            bool actuel = new Controlleur().telephone("0690758861");
            bool expected = true;
            Assert.AreEqual(expected, actuel);
        }


        [TestMethod()]
        public void nomTest()
        {
            bool actuel = new Controlleur().nom("segor");
            bool expected = true;
            Assert.AreEqual(expected, actuel);
        }
        [TestMethod()]
        public void prenomTest()
        {
            bool actuel = new Controlleur().prenom("joackim");
            bool expected = true;
            Assert.AreEqual(expected, actuel);
        }
    }
}