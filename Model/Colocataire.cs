﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Colocataire
    {
        private int id;
        private string nom;
        private string prenom;
        private string mail;
        private string telephone;

        public Colocataire(string nom, string prenom, string mail, string telephone)
        {
            this.nom = nom;
            this.prenom = prenom;
            this.mail = mail;
            this.telephone = telephone;
        }
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }
        public string Mail
        {
            get { return mail; }
            set { mail = value; }
        }
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public override string ToString()
        {
            return string.Format("Nom : {0} / Prénom : {1} / Mail : {2} / Téléphone : {3}", Nom, Prenom, Mail, Telephone);
        }
    }
}
