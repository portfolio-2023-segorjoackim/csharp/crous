﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Depense
    {
        private int id;
        private Colocataire coloc;
        private DateTime date;
        private string text;
        private string justificatif;
        private decimal montant;
        private bool reparti;

        public Depense(DateTime date,string text,string justificatif,decimal montant,Colocataire coloc,bool reparti = false)
        {
            this.date = date;
            this.text = text;
            this.justificatif = justificatif;
            this.montant = montant;
            this.Coloc = coloc;
            this.reparti = reparti;
        }
        public DateTime Date 
        { 
            get { return date; }
            set { date = value; }
        }
        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        public decimal Montant
        {
            get { return montant; }
            set { montant = value; }
        }
        public string Justificatif
        {
            get { return justificatif; }
            set { justificatif = value; }
        }
        public bool Reparti
        {
            get { return reparti; }
            set { reparti = value; }
        }
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public Colocataire Coloc
        {
            get { return coloc; }
            set { coloc = value; }
        }
        public override string ToString()
        {
            return String.Format("Text : {0} / Montant : {1} / Date : {2} / Réparti : {3}", Text,Montant, Date,Reparti);
        }
    }
}
