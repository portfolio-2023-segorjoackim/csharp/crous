﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Controlleur
    {
        private char[] interdit;
        private char[] obligatoire;
        private char[] autoriser;
        public bool nom(string lenom)
        {
            this.interdit = new char[] { '\'', '"', ';', ',', '!', '=', '?', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            foreach (char c in interdit)
            {
                if (lenom.Contains(c) || lenom.Length > 15 || lenom.Length <4)
                {
                    return false;
                }
            }
            return true;
        }
        public bool prenom(string leprenom)
        {
            this.interdit = new char[] { '\'', '"', ';', ',', '!', '=', '?', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            foreach (char c in interdit)
            {
                if (leprenom.Contains(c) || leprenom.Length > 15 || leprenom.Length < 4)
                {
                    return false;
                }
            }
            return true;
        }
        public bool mail(string lemail)
        {
            this.interdit = new char[] { '\'', '"', ';', ',', '!', '=', '?' };
            this.obligatoire = new char[] { '@', '.' };
            foreach(char c in obligatoire)
            {
                if(lemail.Contains(c) == false || lemail.Length < 20)
                {
                    return false;
                }

            }
            foreach(char c in interdit)
            {
                if(lemail.Contains(c) == true) 
                {
                    return false;
                }
            }
            return true;
            

        }
        public bool telephone(string letel)
        {
            this.autoriser = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            foreach(char c in letel)
            {
                if (letel.Contains(c) == false || letel.Length != 10){
                    return false;
                }

            }
            return true;
        }
        public bool prix(string leprix)
        {
            this.autoriser = new char[] {'0','1','2','3','4','5','6','7','8','9',',' };
            foreach(char c in leprix) 
            {
                if (this.autoriser.Contains(c) == false)
                {
                    return false;
                }
            }
            return true;
        }

        public bool validationColoc(string lenom,string leprenom,string lemail,string letel)
        {
            List<bool> list = new List<bool> {nom(lenom),prenom(leprenom),mail(lemail),telephone(letel) };
            foreach(bool b in list)
            {
                if(b == false)
                {
                    return false;
                }
            }
            return true;
        }
        public bool text(string leprenom)
        {
            this.interdit = new char[] { '\'', '"', ';', ',', '!', '=', '?','@','\\' };
            foreach (char c in interdit)
            {
                if (leprenom.Contains(c) || leprenom.Length > 20 || leprenom.Length < 4)
                {
                    return false;
                }
            }
            return true;
        }

        public bool validationDepense(string texte,string montant)
        {
            List<bool> list = new List<bool> {text(texte),prix(montant) };
            foreach (bool b in list)
            {
                if (b == false)
                {
                    return false;
                }
            }
            return true;
        }
    }

}

