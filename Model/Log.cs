﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Log
    {
        private int identifiant;
        private string action;
        private string address;
        private DateTime time;

        public Log(string action, string address, DateTime time)
        {
            this.identifiant = 1;
            this.action = action;
            this.address = address;
            this.time = time;
        }
        public int Identifiant
        {
            get { return identifiant; }
        }
        public string Action
        {
            get { return action; }
        }

        public string Address
        {
            get { return address; }
        }

        public DateTime Time
        {
            get { return time; }
        }

        public override string ToString()
        {
            return String.Format("L'identifiant : {0} a effectué {1} avec cette adresse ip {2} le {3}", Identifiant, Action, Address, Time);
        }
    }
}
