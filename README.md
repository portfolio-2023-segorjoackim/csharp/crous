# ***Cas d'utilisation de l'application Crous***

>## Sommaire
> * Cahier des charges de l'application
> * Comment l'utiliser ?
> * Les  codes importants au bon fonctionnement de l'application


## 1.Cahier des charges de l'application.
> Objectifs :
> * Créer des fenêtres simples d'utilisation
> * L'utilisateur doit pouvoir ajouter,modifier et supprimer des colocataires
> * L'utilisateur doit pouvoir ajouter,modifier et supprimer des dépenses aux colocataires
> * L'utilisateur doit pouvoir simuler la répartion les dépenses
> * L'utilisateur doit pouvoir répartir les dépenses 
### **GÉRER UN COLOCATAIRE**
--------------
```plantuml
left to right direction
:Colocataire:
package GérerColocataire{
Colocataire--(Consulter la liste des Colocataires)
Colocataire--(Ajouter Colocataire)
Colocataire--(Modifier Colocataire)
Colocataire--(Supprimer Colocataire)
}
```
### **ENREGISTREMENT DES DÉPENSES**
--------------
```plantuml
left to right direction
:Colocataire:
package EnregistrerUneDépense{
Colocataire--(Consulter les dépenses)
Colocataire--(Saisir une dépense)
Colocataire--(Modifier une dépense)
Colocataire--(Supprimer une dépense)
(Saisir une dépense)..(Sélectionne un fichier):<extends>
(Modifier une dépense)<..(Sélectionne un fichier):<extends>
}
```
### **RÉPARTIR UNE DÉPENSE**
---

```plantuml
left to right direction
:Colocataire:
package RepartirDepense{
Colocataire--(Lancer la répartition)
(Lancer la répartition)..>(Calculer la répartition):<include>
(Lancer la répartition)..>(Visualiser le résultat de la répartition):<include>
}
```  

### **SOLDER UNE PÉRIODE**
```plantuml
left to right direction
:Colocataire:
package SolderPériode{
Colocataire--(Solder une période)
(Solder une période)..>(Les dépenses sont définis comme étant répartis):<include>
(Solder une période)..>(Lancer la répartition)
}
```
---

## **2.Guide d'utilisation.**

### Ouvrir l'application

![Menu.png](assets/img/fenetre-menu.png)

---

### Créer des colocataires
>Cliquez sur le bouton Gérer les colocataires.  
La fenêtre ci-desssous s'ouvrira.  

![GérerColoc.png](assets/img/gerer-coloc.png)
>Cliquez sur le bouton Ajouter.  
Une autre fenêtre s'ouvrira.

![AjouterColoc.png](assets/img/ajouter-coloc.png)
>Vous serez donc obligé de remplir les champs de texte vide.  
Lorsque vous aurez fini cliquer sur le bouton Ajouter.

# /!\ ATTENTION /!\
>Si lorsque vous appuyez sur le bouton Ajouter un message s'affiche.  
Vérifier que vous ayez correctement rempli les champs.
>* Les champs Nom et Prénom ne prennent pas de chiffres ni de caractères s
péciaux et le nombre de caractères doit être supérieur à 3 et inférieur à 15.  
>* Le champ Mail doit contenir au moins 20 caractères un @ et un . et n'accepte pas d'autres caractères spéciaux.  
>* Le champ Téléphone doit contenir que des chiffres et doit obligatoirement en contenir 10.  

### Modifier le colocataire

>Selectionner un colocataire en cliquant dessus.

![SelectionColoc.png](assets/img/gerer-coloc-modifier.png)
>Puis cliquez sur modifier le principe est le même que pour l'ajout du colocataire.  
### Supprimer le colocataire

>Pour supprimer comme précédemment on selectionne le colocataire puis on clique sur Supprimer.

---

### Créer des dépenses pour les colocataires

![AjoutDépense](assets/img/ajoutD.jpg)

### Simuler la répartition 
Simuler la répartition permet de savoir ce que les colocataires doivent ou pas sans répartir les dépenses

![SimulerRépartition](assets/img/repartir.jpg)

### Répartir les dépenses
Lorsque l'on répartit les dépenses on considère que tout le monde à régler ce qu'il devait donc les dépenses n'apparaissent plus dans la fenêtre gérer les dépenses

![RépartirDépenses](assets/img/solder.jpg)

## **3.Les codes veillants au bon fonctionnement de l'application.**  
 
 --- 

### **LA BASE DE DONNÉE**
```MySql
create database crous;
use crous;
```

```MySql
CREATE TABLE IF NOT EXISTS colocataire (
  id int(11) NOT NULL AUTO_INCREMENT,
  nom varchar(50) DEFAULT NULL,
  prenom varchar(50) DEFAULT NULL,
  mail varchar(50) DEFAULT NULL,
  telephone varchar(20) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id (id)
) ENGINE= innodb;
```

```MySql
CREATE TABLE IF NOT EXISTS depense (
  id int(11) NOT NULL AUTO_INCREMENT,
  date date DEFAULT NULL,
  text varchar(50) DEFAULT NULL,
  justificatif varchar(250) DEFAULT NULL,
  montant decimal(10,2) DEFAULT NULL,
  repartir tinyint(4) DEFAULT NULL,
  idColocataire int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (id),
  KEY id (id),
  KEY idx_idColocataire (idColocataire)
) ENGINE= innodb;
```

```MySql
CREATE TABLE IF NOT EXISTS logs (
  id int(11) NOT NULL AUTO_INCREMENT,
  identifiant int(11) UNSIGNED NOT NULL,
  action varchar(50) DEFAULT NULL,
  adresse varchar(250) DEFAULT NULL,
  time datetime DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id (id),
  KEY idx_idColocataire (identifiant)
) ENGINE= innodb;
```



