﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Model;
using System.Runtime.InteropServices;

namespace Dao
{
    public class DaoDepense:DaoConnector
    {
        private List<Depense> depenseList = new List<Depense>();
        public DaoDepense()
        {
            initAllDepense();
        }
        public void addDepense(Depense depense)
        {
            this.depenseList.Add(depense);
            string req = string.Format("insert into depense(date,text,justificatif,montant,repartir,idColocataire) values(@date,@text,@justificatif,@montant,@repartir,@idColocataire)");
            MySqlCommand addD = new MySqlCommand(req, getConnection());
            addD.Parameters.Add(new MySqlParameter("@date", depense.Date));
            addD.Parameters.Add(new MySqlParameter("@text", depense.Text));
            addD.Parameters.Add(new MySqlParameter("@justificatif", depense.Justificatif));
            addD.Parameters.Add(new MySqlParameter("@montant", depense.Montant));
            addD.Parameters.Add(new MySqlParameter("@repartir", Convert.ToInt16(depense.Reparti)));
            addD.Parameters.Add(new MySqlParameter("@idColocataire", depense.Coloc.Id));
            addD.Parameters.Add(new MySqlParameter("@id", depense.Id));
            addD.ExecuteNonQuery();
            outConnection();
        }

        private void initAllDepense()
        {
            string req = "select * from depense";
            MySqlCommand cmd = new MySqlCommand(req, getConnection());
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                DateTime date = (DateTime)rdr["date"];
                string text = (string)rdr["text"];
                string justificatif = (string)rdr["justificatif"];
                decimal montant = Convert.ToDecimal(rdr["montant"]);
                Colocataire colocataire = new DaoColoc().getColocataire(Convert.ToInt16(rdr["idColocataire"]));
                bool repartir = Convert.ToBoolean(rdr["repartir"]);
                Depense depense = new Depense(date,text ,justificatif ,montant ,colocataire,repartir);
                depense.Id = (int)rdr["id"];
                this.depenseList.Add(depense);
            }
            rdr.Close();
            outConnection();
        }
        public void editDepense(Depense laDepense)
        {
            string req = String.Format("UPDATE depense SET date =@date ,text =@text ,justificatif = @justificatif,montant = @montant,repartir = @repartir,idColocataire = @idColocataire WHERE id = @id");
            MySqlCommand cmd = new MySqlCommand(req, getConnection());
            cmd.Parameters.Add(new MySqlParameter("@date", laDepense.Date));
            cmd.Parameters.Add(new MySqlParameter("@text", laDepense.Text));
            cmd.Parameters.Add(new MySqlParameter("@justificatif", laDepense.Justificatif));
            cmd.Parameters.Add(new MySqlParameter("@montant", laDepense.Montant));
            cmd.Parameters.Add(new MySqlParameter("@repartir", Convert.ToInt16(laDepense.Reparti)));
            cmd.Parameters.Add(new MySqlParameter("@idColocataire", laDepense.Coloc.Id));
            cmd.Parameters.Add(new MySqlParameter("@id", laDepense.Id));
            cmd.ExecuteNonQuery();
            outConnection();

        }
        public void deleteDepense(int id)
        {
            string req = string.Format("delete from depense where id = {0}", id);
            MySqlCommand cmd = new MySqlCommand(req, getConnection());
            cmd.ExecuteNonQuery();
            outConnection();

        }
        public List<Depense> getAllDepense()
        {
            return this.depenseList;
        }
        public decimal PriceTotal()
        {
            decimal total = 0.0m;
            foreach (Depense d in this.depenseList)
            {
                if(d.Reparti == false)
                {
                    total += d.Montant;
                }
                
            }
            return total;
        }
        public decimal PriceTotalColoc(Colocataire coloc) 
        {
            decimal total = 0.0m;
            foreach(Depense d in this.depenseList)
            {
                if(d.Coloc.Id == coloc.Id && d.Reparti == false)
                {
                    total += d.Montant;
                }
            }
            return total;
        }
        public decimal Repartition()
        {
            int count = new DaoColoc().getAllColoc().Count();
            decimal total = PriceTotal()/count;
            return total;
        }
    }
}
