﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Model;
using System.Security.Permissions;

namespace Dao
{
    public class DaoColoc:DaoConnector
    {
        private List<Colocataire> colocataireList = new List<Colocataire>();   
        public DaoColoc() 
        {
            initAllColoc();
        }
        public void addColoc(Colocataire coloc)
        {
            colocataireList.Add(coloc);
            string req = string.Format("insert into colocataire(nom,prenom,mail,telephone) values('{0}','{1}','{2}','{3}')",coloc.Nom,coloc.Prenom,coloc.Mail,coloc.Telephone);
            MySqlCommand addC= new MySqlCommand(req,getConnection());
            addC.ExecuteNonQuery();
            outConnection();
            


           

        }
        private void initAllColoc()
        {
            string req = "select * from colocataire";
            MySqlCommand cmd = new MySqlCommand(req,getConnection());
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {    
                Colocataire coloc = new Colocataire((string)rdr["nom"],(string)rdr["prenom"],(string)rdr["mail"],(string)rdr["telephone"]);
                coloc.Id = (int)rdr["id"];
                colocataireList.Add(coloc);
            }
            rdr.Close();
            outConnection();
        }
        public void editColoc(Colocataire leColoc)
        {
            string req = String.Format("UPDATE colocataire SET nom ='{0}' ,prenom ='{1}' ,mail = '{2}',telephone = '{3}' WHERE id = {4}",leColoc.Nom,leColoc.Prenom,leColoc.Mail,leColoc.Telephone,leColoc.Id);
            MySqlCommand cmd = new MySqlCommand(req,getConnection());
            cmd.ExecuteNonQuery();
            outConnection();
            
        }
        public void deleteColoc(int id)
        {
            string req = string.Format("delete from colocataire where id = {0}", id);
            string req2 = string.Format("delete from depense where idColocataire = {0}", id);
            MySqlCommand cmd = new MySqlCommand(req,getConnection());
            cmd.ExecuteNonQuery();
            List<Depense> daoDepense = new DaoDepense().getAllDepense();
            foreach (Depense depense in daoDepense)
            {
                if(depense.Coloc.Id == id)
                {
                    MySqlCommand cmd2 = new MySqlCommand(req2, getConnection());
                    cmd2.ExecuteNonQuery();
                }
            }
            outConnection();

        }
        public Colocataire getColocataire(int id)
        {
            List<Colocataire> leColoc = new List<Colocataire>();
            foreach(Colocataire coloc in this.colocataireList)
            {
                if( coloc.Id == id)
                {
                    leColoc.Add(coloc);
                }
            }
            return leColoc[0];   
            
        }
        public List<Colocataire> getAllColoc()
        {
            return colocataireList;
        }
    
    }
}
