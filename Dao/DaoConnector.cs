﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Dao
{
    public class DaoConnector
    {

        private string host = "user=root;password=;server=localhost;database=crous";
        private MySqlConnection conn;
        public DaoConnector()
        {
        }

        public MySqlConnection  getConnection()
        {
            conn = new MySqlConnection(host);
            conn.Open();
            return conn;
        }

        public void outConnection()
        {
            conn.Close();
        }
    }
}
