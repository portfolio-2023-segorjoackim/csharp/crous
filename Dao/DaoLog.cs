﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Dao
{
    public class DaoLog:DaoConnector
    {
        public void addLog(Log log)
        {
            string req = string.Format("insert into logs(identifiant,action,adresse,time) values(@identifiant,@action,@adresse,@time)");
            //Console.WriteLine(log.Address.ToString());
            MySqlCommand addL = new MySqlCommand(req, getConnection());
            addL.Parameters.Add(new MySqlParameter("@identifiant",log.Identifiant));
            addL.Parameters.Add(new MySqlParameter("@action", log.Action));
            addL.Parameters.Add(new MySqlParameter("@adresse", log.Address));
            addL.Parameters.Add(new MySqlParameter("@time", log.Time));
            addL.ExecuteNonQuery();
            outConnection();

        }
    }
    
}
